package com.praxis16.tac.praxis2k16;

import android.app.Fragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Window;
import android.view.WindowManager;

import com.praxis16.tac.praxis2k16.AboutUs.AboutUs_main;
import com.praxis16.tac.praxis2k16.Events.Events_main;
import com.praxis16.tac.praxis2k16.FbNews.Fb_main;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabSelectListener;

public class MainActivity extends AppCompatActivity {

    BottomBar bottomBar;
    Fragment fragment=null;
    static boolean tab=false;
    static String cat="";


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("News | Praxis '16");

        bottomBar = (BottomBar)findViewById(R.id.bottombar);

        bottomBar.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelected(@IdRes int tabId) {
                switch (tabId){

                    case R.id.tab_events:
                        Events_main events_main = new Events_main();
                        getFragmentManager().beginTransaction().replace(R.id.main_fragment, events_main).commit();

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            Window window = getWindow();
                            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                            window.setStatusBarColor(Color.rgb(27,94,32));
                            window.setNavigationBarColor(Color.rgb(27,94,32));
                        }

                        toolbar.setBackground(new ColorDrawable(Color.rgb(76,175,80)));
                        toolbar.setTitle("Events | Praxis '16");
                        break;
                    case R.id.tab_news:
                        Fb_main fb_main = new Fb_main();
                        getFragmentManager().beginTransaction().replace(R.id.main_fragment, fb_main).commit();

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            Window window = getWindow();
                            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                            window.setStatusBarColor(Color.rgb(26,35,126));
                            window.setNavigationBarColor(Color.rgb(26,35,126));
                        }

                        toolbar.setBackground(new ColorDrawable(Color.rgb(63,81,181)));
                        toolbar.setTitle("News | Praxis '16");
                        break;
                    case R.id.tab_about:
                        AboutUs_main aboutUs_main = new AboutUs_main();
                        getFragmentManager().beginTransaction().replace(R.id.main_fragment, aboutUs_main).commit();

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            Window window = getWindow();
                            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                            window.setStatusBarColor(Color.rgb(69,90,100));
                            window.setNavigationBarColor(Color.rgb(69,90,100));
                        }

                        toolbar.setBackground(new ColorDrawable(Color.rgb(96,125,139)));
                        toolbar.setTitle("About Us | Praxis '16");
                        break;
                }
            }
        });
        if(!tab) {
            bottomBar.selectTabWithId(R.id.tab_news);
        }else{
            bottomBar.selectTabWithId(R.id.tab_events);
        }
    }
}
